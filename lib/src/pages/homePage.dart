import 'package:flutter/material.dart';
import 'package:userprofile/src/pages/firstPage.dart';
import 'package:userprofile/src/pages/secondPage.dart';
import 'package:userprofile/src/pages/thirdPage.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin{
  late TabController controller;
  
  @override
  void initState() {
    // TODO: implement initState
    controller = TabController(length: 3, vsync: this);
    controller.addListener(() {
      setState(() {

      });
    });
    super.initState();
  }

  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // title: Text('Update My Profile ${controller.index +1}'),
        backgroundColor: Color(0xFF90A4AE),
        title: Text('Update My Profile'),
        bottom: TabBar(
          controller: controller,
            tabs: [
          Tab(
            text: 'Basic Data',
          ),
          Tab(
            text: 'National Identity',
          ),
          Tab(
            text: 'Academy',
          ),
          //     Tab(
          //       child: Text('Basic Data', style: TextStyle(fontWeight: FontWeight.bold),),
          //     ),
          //     Tab(
          //
          //       child: Text('National Identity', style: TextStyle( fontWeight: FontWeight.bold),),
          //     ),
          //     Tab(
          //
          //       child: Text('Academy', style: TextStyle( fontWeight: FontWeight.bold),),
          //     ),

        ]),
      ),
      body: TabBarView(
          controller: controller,
          children: [
        // Center(
        //   child: Text('Tab 1 Content'),
        // ),
        // Center(
        //   child: Text('Tab 2 Content'),
        // ),
        // Center(
        //   child: Text('Tab 3 Content'),
        // ),
        Center(
          child: FirstPage(),
        ),
        Center(
          child: SecondPage(),
        ),
        Center(
          child: ThirdPage(),
        ),

      ]),
    );
  }
}
