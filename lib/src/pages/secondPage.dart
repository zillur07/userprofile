import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class SecondPage extends StatefulWidget {
  const SecondPage({Key? key}) : super(key: key);

  @override
  State<SecondPage> createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  final officeAddressController = TextEditingController();
  final nidNumberController = TextEditingController();
  final PasportNumberController = TextEditingController();
  final issurPicController = TextEditingController();

  String dropdownvalue = 'Zone-1';
  String taxvalue = 'Circle-140';
  var values = [
    'Circle-140',
    'Circle-141',
    'Circle-142',
    'Circle-143',
    'Circle-144',
    'Circle-145',
    'Circle-146',
    'Circle-147',
    'Circle-148',
    'Circle-149',
    "Circle-150",
  ];
  var items = [
    'Zone-1',
    'Zone-2',
    'Zone-3',
    'Zone-4',
    'Zone-5',
    'Zone-6',
    'Zone-7',
    'Zone-8',
    'Zone-9',
    'Zone-10',
  ];
  DateTime? pickedDate;
  String? expdDate;
  String? issuDate;
  File? _nidF;
  File? _nidB;
  File? _pass;
  File? _birth;
  File? _tax;
  Future getImage(ImageSource source, String pic) async {
    final image = await ImagePicker().pickImage(
      source: source,
    );
    if (image == null) return;
    final saveImg = await saveImage(image.path);

    setState(() {
      pic == "NF"
          ? this._nidF = saveImg
          : pic == "NB"
              ? this._nidB = saveImg
              : pic == "B"
                  ? this._birth = saveImg
                  : pic == "pass"
                      ? this._pass = saveImg
                      : this._tax = saveImg;
    });
  }

  Future saveImage(String imagePath) async {
    final directory = await getApplicationDocumentsDirectory();
    final name = basename(imagePath);
    final image = File("${directory.path}/$name");
    return File(imagePath).copy(image.path);
  }

  pickDate(BuildContext context, String date) async {
    pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime.now());
    if (pickedDate != null) {
      date == "exp"
          ? expdDate = DateFormat.yMMMd().format(pickedDate!)
          : issuDate = DateFormat.yMMMd().format(pickedDate!);
      setState(() {});
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              decoration: InputDecoration(
                label: Text("Office Address"),
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                label: Text("NID Number"),
              ),
            ),
            Row(
              children: [
                Text(
                  "NID Front Side ",
                ),
                IconButton(
                    onPressed: () {
                      getImage(ImageSource.camera, "NF");
                    },
                    icon: Icon(Icons.camera_alt)),
                IconButton(
                    onPressed: () {
                      getImage(ImageSource.gallery, "NF");
                    },
                    icon: Icon(Icons.photo)),
              ],
            ),
            _nidF != null
                ? Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    height: 150,
                    width: 250,
                    child: Image.file(
                      _nidF!,
                      fit: BoxFit.cover,
                    ),
                  )
                : SizedBox(),
            Row(
              children: [
                Text(
                  "NID Back Side ",
                ),
                IconButton(
                    onPressed: () {
                      getImage(ImageSource.camera, "NB");
                    },
                    icon: Icon(Icons.camera_alt)),
                IconButton(
                    onPressed: () {
                      getImage(ImageSource.gallery, "NB");
                    },
                    icon: Icon(Icons.photo)),
              ],
            ),
            _nidB != null
                ? Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    height: 150,
                    width: 250,
                    child: Image.file(
                      _nidB!,
                      fit: BoxFit.cover,
                    ),
                  )
                : SizedBox(),
            TextFormField(
              decoration: InputDecoration(
                label: Text("Passport No"),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: issuDate != null
                      ? Text("${issuDate}")
                      : Text("Choose Date"),
                ),
                ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(
                      primary: Color(0xFF90A4AE),
                    ),
                    onPressed: () => pickDate(context, ""),
                    icon: Icon(Icons.calendar_month),
                    label: Text("Passport Issue Date"))
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: expdDate != null
                      ? Text("${expdDate}")
                      : Text("Choose Date"),
                ),
                ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(
                      primary: Color(0xFF90A4AE),
                    ),
                    onPressed: () => pickDate(context, "exp"),
                    icon: Icon(Icons.calendar_month),
                    label: Text("Passport Exp Date"))
              ],
            ),
            TextFormField(
              controller: issurPicController,
              decoration: const InputDecoration(

                label: Text("Issue Place"),
              ),
            ),
            Row(
              children: [
                Text(
                  "Passport Copy ",
                ),
                IconButton(
                    onPressed: () {
                      getImage(ImageSource.camera, "pass");
                    },
                    icon: Icon(Icons.camera_alt)),
                IconButton(
                    onPressed: () {
                      getImage(ImageSource.gallery, "pass");
                    },
                    icon: Icon(Icons.photo)),
              ],
            ),
            _pass != null
                ? Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    height: 150,
                    width: 250,
                    child: Image.file(
                      _pass!,
                      fit: BoxFit.cover,
                    ),
                  )
                : SizedBox(),
            TextFormField(
              decoration: const InputDecoration(
                label: Text("Birth Certificate Number"),
              ),
            ),
            Row(
              children: [
                Text(
                  "Birth Certificate Copy ",
                ),
                IconButton(
                    onPressed: () {
                      getImage(ImageSource.camera, "B");
                    },
                    icon: Icon(Icons.camera_alt)),
                IconButton(
                    onPressed: () {
                      getImage(ImageSource.gallery, "B");
                    },
                    icon: Icon(Icons.photo)),
              ],
            ),
            _birth != null
                ? Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    height: 150,
                    width: 250,
                    child: Image.file(
                      _birth!,
                      fit: BoxFit.cover,
                    ),
                  )
                : SizedBox(),
            TextFormField(
              decoration: const InputDecoration(
                label: Text("Income Tax ID"),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Tax Zone"),
                DropdownButton(
                  value: dropdownvalue,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  items: items.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Container(
                          padding: EdgeInsets.only(right: 40),
                          child: Text(items)),
                    );
                  }).toList(),
                  onChanged: (String? newValue) {
                    setState(() {
                      dropdownvalue = newValue!;
                    });
                  },
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Tax Circle"),
                DropdownButton(
                  value: taxvalue,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  items: values.map((String values) {
                    return DropdownMenuItem(
                      value: values,
                      child: Container(
                          padding: EdgeInsets.only(right: 40),
                          child: Text(values)),
                    );
                  }).toList(),
                  onChanged: (String? newValue) {
                    setState(() {
                      taxvalue = newValue!;
                    });
                  },
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  "Tax Certificate Copy ",
                ),
                IconButton(
                    onPressed: () {
                      getImage(ImageSource.camera, "");
                    },
                    icon: Icon(Icons.camera_alt)),
                IconButton(
                    onPressed: () {
                      getImage(ImageSource.gallery, "");
                    },
                    icon: Icon(Icons.photo)),
              ],
            ),
            _tax != null
                ? Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    height: 150,
                    width: 250,
                    child: Image.file(
                      _tax!,
                      fit: BoxFit.cover,
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
