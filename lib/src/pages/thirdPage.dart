import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class ThirdPage extends StatefulWidget {
  const ThirdPage({Key? key}) : super(key: key);

  @override
  State<ThirdPage> createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage> {

  TextEditingController masters=TextEditingController();
  String dropdownvalue = 'Science';
  String dropdown = 'Science';
  var items = [
    'Science',
    'Human Studies',
    'Business Studies',
    'Others',
  ];
  bool isChecked=false;
  bool isCheck=false;
  bool isHsc=false;
  bool isSsc=false;
  DateTime? pickedDate;
  String ?expdDate;
  String? issuDate;
  File? _mas;
  File? _bsc;
  File? _ssc;
  File? _hsc;
  Future getImage(ImageSource source,String pic) async {
    final image = await ImagePicker().pickImage(
      source: source,
    );
    if (image == null) return;
    final saveImg = await saveImage(image.path);

    setState(() {
      pic=="MS" ? this._mas = saveImg:pic=="bsc"?this._bsc=saveImg:pic=="hsc"?this._hsc=saveImg:this._ssc=saveImg;
    });
  }

  Future saveImage(String imagePath) async {
    final directory = await getApplicationDocumentsDirectory();
    final name = basename(imagePath);
    final image = File("${directory.path}/$name");
    return File(imagePath).copy(image.path);
  }

  pickDate(BuildContext context,String date)async{
    pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime.now());
    if (pickedDate != null) {

      date=="exp"?expdDate =DateFormat.yMMMd().format(pickedDate!):issuDate=DateFormat.yMMMd().format(pickedDate!);
      setState(() {

      });
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return  SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Checkbox(
                      value: isChecked,
                      onChanged: (bool? value){
                        setState(() {
                          isChecked=!isChecked;
                        });
                      },
                    ),
                    Text("Masters Passing Year"),
                  ],
                ),

                SizedBox(
                  width: 100,
                  child: TextFormField(
                    enabled: isChecked?true:false,
                    textAlign: TextAlign.end,
                    //   readOnly: isChecked?false:true,
                    initialValue: "0",
                    decoration: InputDecoration(
                    ),
                  ),
                ),
              ],
            ),

            TextFormField(
              enabled: isChecked?true:false,
              decoration: InputDecoration(
                label: Text("University/Institution"),
              ),
            ),
            TextFormField(
              enabled: isChecked?true:false,
              decoration: InputDecoration(
                label: Text("Subject Name"),
              ),
            ),
            TextFormField(
              enabled: isChecked?true:false,
              decoration: InputDecoration(
                label: Text("Certificate Number"),
              ),
            ),
            Row(
              children: [
                Text(
                  "Certificate Copy ",
                ),
                IconButton(onPressed:isChecked? (){ getImage(ImageSource.camera,"MS");}:(){}, icon: Icon(Icons.camera_alt)),
                IconButton(onPressed: isChecked?(){ getImage(ImageSource.gallery,"MS");}:(){}, icon: Icon(Icons.photo)),
              ],
            ),
            _mas!=null?   Container(
              margin: EdgeInsets.symmetric(horizontal: 30),
              height: 150,
              width: 250,
              child:Image.file(_mas!,fit: BoxFit.cover,),
            ):SizedBox(),


            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Checkbox(
                      value: isCheck,
                      onChanged: (bool? value){
                        setState(() {
                          isCheck=!isCheck;
                        });
                      },
                    ),
                    Text("Bachelor Passing Year"),
                  ],
                ),

                SizedBox(
                  width: 100,
                  child: TextFormField(
                    enabled: isCheck?true:false,
                    textAlign: TextAlign.end,
                    //   readOnly: isChecked?false:true,
                    initialValue: "0",
                    decoration: InputDecoration(
                    ),
                  ),
                ),
              ],
            ),

            TextFormField(
              enabled: isCheck?true:false,
              decoration: InputDecoration(
                label: Text("University/Institution"),
              ),
            ),
            TextFormField(
              enabled: isCheck?true:false,
              decoration: InputDecoration(
                label: Text("Subject Name"),
              ),
            ),
            TextFormField(
              enabled: isCheck?true:false,
              decoration: InputDecoration(
                label: Text("Certificate Number"),
              ),
            ),
            Row(
              children: [
                Text(
                  "Certificate Copy ",
                ),
                IconButton(onPressed:isCheck? (){ getImage(ImageSource.camera,"bsc");}:(){}, icon: Icon(Icons.camera_alt)),
                IconButton(onPressed:isCheck? (){ getImage(ImageSource.gallery,"bsc");}:(){}, icon: Icon(Icons.photo)),
              ],
            ),
            _bsc!=null?   Container(
              margin: EdgeInsets.symmetric(horizontal: 30),
              height: 150,
              width: 250,
              child:Image.file(_bsc!,fit: BoxFit.cover,),
            ):SizedBox(),



            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Checkbox(
                      value: isHsc,
                      onChanged: (bool? value){
                        setState(() {
                          isHsc=!isHsc;
                        });
                      },
                    ),
                    Text("HSC Passing Year"),
                  ],
                ),

                SizedBox(
                  width: 100,
                  child: TextFormField(
                    enabled: isHsc?true:false,
                    textAlign: TextAlign.end,
                    //   readOnly: isChecked?false:true,
                    initialValue: "0",
                    decoration: InputDecoration(
                    ),
                  ),
                ),
              ],
            ),

            TextFormField(
              enabled: isHsc?true:false,
              decoration: InputDecoration(
                label: Text("University/Institution"),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Group Name"),
                DropdownButton(
                  value: dropdown,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  items: items.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Container(
                          padding: EdgeInsets.only(right: 40),
                          child: Text(items)),
                    );
                  }).toList(),
                  onChanged:isHsc? (String? newValue) {
                    setState(() {
                      dropdown = newValue!;
                    });
                  }:null,
                ),
              ],),

            TextFormField(
              enabled: isHsc?true:false,
              decoration: InputDecoration(
                label: Text("Certificate Number"),
              ),
            ),
            Row(
              children: [
                Text(
                  "Certificate Copy ",
                ),
                IconButton(onPressed:isHsc? (){ getImage(ImageSource.camera,"hsc");}:(){}, icon: Icon(Icons.camera_alt)),
                IconButton(onPressed:isHsc? (){ getImage(ImageSource.gallery,"hsc");}:(){}, icon: Icon(Icons.photo)),
              ],
            ),
            _hsc!=null?   Container(
              margin: EdgeInsets.symmetric(horizontal: 30),
              height: 150,
              width: 250,
              child:Image.file(_hsc!,fit: BoxFit.cover,),
            ):SizedBox(),



            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Checkbox(
                      value: isSsc,
                      onChanged: (bool? value){
                        setState(() {
                          isSsc=!isSsc;
                        });
                      },
                    ),
                    Text("SSC Passing Year"),
                  ],
                ),

                SizedBox(
                  width: 100,
                  child: TextFormField(
                    enabled: isSsc?true:false,
                    textAlign: TextAlign.end,
                    //   readOnly: isChecked?false:true,
                    initialValue: "0",
                    decoration: InputDecoration(
                    ),
                  ),
                ),
              ],
            ),

            TextFormField(
              enabled: isSsc?true:false,
              decoration: InputDecoration(
                label: Text("University/Institution"),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Group Name"),
                DropdownButton(
                  value: dropdownvalue,
                  icon: const Icon(Icons.keyboard_arrow_down),
                  items: items.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Container(
                          padding: EdgeInsets.only(right: 40),
                          child: Text(items)),
                    );
                  }).toList(),
                  onChanged:isSsc? (String? newValue) {
                    setState(() {
                      dropdownvalue = newValue!;
                    });
                  }:null,
                ),
              ],),

            TextFormField(
              enabled: isSsc?true:false,
              decoration: InputDecoration(
                label: Text("Certificate Number"),
              ),
            ),
            Row(
              children: [
                Text(
                  "Certificate Copy ",
                ),
                IconButton(onPressed:isSsc? (){ getImage(ImageSource.camera,"");}:(){}, icon: Icon(Icons.camera_alt)),
                IconButton(onPressed:isSsc? (){ getImage(ImageSource.gallery,"");}:(){}, icon: Icon(Icons.photo)),
              ],
            ),
            _ssc!=null?   Container(
              margin: EdgeInsets.symmetric(horizontal: 30),
              height: 150,
              width: 250,
              child:Image.file(_ssc!,fit: BoxFit.cover,),
            ):SizedBox(),

          ],
        ),
      ),
    );

  }
}
