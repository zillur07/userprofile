import 'dart:io';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class FirstPage extends StatefulWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  // File? _image;
  //
  // Future pickImage(ImageSource source) async {
  //   try {
  //     final image = await ImagePicker().pickImage(source: source);
  //     if (image == null) return;
  //     //final imageTemporary = File(image.path);
  //     final imagePermanent = await saveImagePermanently(image.path);
  //     setState(
  //       () {
  //         this._image = imagePermanent;
  //       },
  //     );
  //   } on PlatformException catch (e) {
  //     print('Failed to pick Image : $e');
  //   }
  // }
  //
  // Future<File> saveImagePermanently(String imagePath) async{
  //   final directory = await getApplicationDocumentsDirectory();
  //   final name = basename(imagePath);
  //   final image = File('${directory.path}/ $name');
  //   return File(image.path).copy(image.path);
  // }

  File? _image;
  String? ocupation;
  Future getImage(ImageSource source) async {
    final image = await ImagePicker().pickImage(
      source: source,
    );
    if (image == null) return;
    final saveImg = await saveImage(image.path);

    setState(() {
      this._image = saveImg;
    });
  }

  Future<File> saveImage(String imagePath) async {
    final directory = await getApplicationDocumentsDirectory();
    final name = basename(imagePath);
    final image = File("${directory.path}/$name");
    return File(imagePath).copy(image.path);
  }

  final nameController = TextEditingController();
  final phoneController = TextEditingController();
  final fatherNameController = TextEditingController();
  final addressController = TextEditingController();
  final motherNameController = TextEditingController();
  final designationNameController = TextEditingController();

  String gender = 'Gender Change';
  String empolye = 'Empolye Change';
  String occupation = 'Occopation Change';
  // late DateTime _dateTime;
  //
  // getDate()  {
  //   Future<DateTime?> date = showDatePicker(
  //     context: context,
  //     initialDate: DateTime(DateTime.now().year),
  //     firstDate: DateTime(DateTime.now().year - 20),
  //     lastDate: DateTime(DateTime.now().year + 3),
  //   );
  //
  //   setState(() {
  //     _dateTime = date as DateTime;
  //   });
  // }

  DateTime? _selectedDate;

  void _presentDatePicker(BuildContext context) {
    // showDatePicker is a pre-made funtion of Flutter
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2080),



    ).then((pickedDate) {
      // Check if no date is selected
      if (pickedDate == null) {
        return;
      }
      setState(() {
        // using state so that the UI will be rerendered when date is picked
        _selectedDate = pickedDate;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Stack(
                children: [
                  Spacer(),
                  Center(
                    child: Stack(children: [
                      CircleAvatar(
                        radius: 50,
                        backgroundColor: Color(0xFF90A4AE),
                        backgroundImage: _image != null
                            ? FileImage(
                          _image!,
                        )
                            : null,
                      ),
                      Positioned(
                          right: 0,
                          bottom: 10,
                          child: GestureDetector(
                            onTap: () {
                              getImage(ImageSource.camera);
                            },
                            child: Icon(Icons.camera_alt),
                          ))
                    ]),
                  ),

                ],
              ),
            ),
            TextField(
              controller: nameController,
              // onChanged: contactController.name,
              decoration: InputDecoration(
                // focusedBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.purple, width: 2.0),
                // ),
                // enabledBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.grey, width: 1.0),
                // ),
                labelText: 'Full Name',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: phoneController,
              // onChanged: contactController.phoneNumber,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                // focusedBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.purple, width: 2.0),
                // ),
                // enabledBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.grey, width: 1.0),
                // ),
                labelText: 'Phone Number',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: nameController,
              // onChanged: contactController.name,
              decoration: InputDecoration(
                // focusedBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.purple, width: 2.0),
                // ),
                // enabledBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.grey, width: 1.0),
                // ),
                labelText: 'Address',
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Icon(Icons.add_circle_outline),
                SizedBox(
                  width: 6,
                ),
                Text('Select Your Country:'),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Text('Gender:'),
                Radio(
                  value: 'Male',
                  groupValue: gender,
                  onChanged: (value) {
                    setState(() {
                      gender = value.toString();
                    });
                  },
                ),
                Text('Mele'),
                Radio(
                  value: 'Female',
                  groupValue: gender,
                  onChanged: (value) {
                    setState(() {
                      gender = value.toString();
                    });
                  },
                ),
                Text('Femele'),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            TextField(
              controller: fatherNameController,
              // onChanged: contactController.name,
              decoration: InputDecoration(
                // focusedBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.purple, width: 2.0),
                // ),
                // enabledBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.grey, width: 1.0),
                // ),
                labelText: 'Father Name',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: motherNameController,
              // onChanged: contactController.name,
              decoration: InputDecoration(
                // focusedBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.purple, width: 2.0),
                // ),
                // enabledBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.grey, width: 1.0),
                // ),
                labelText: 'Mother Name',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text('Are you Employed:'),
                Radio(
                  value: 'Male',
                  groupValue: empolye,
                  onChanged: (value) {
                    setState(() {
                      empolye = value.toString();
                    });
                  },
                ),
                Text('Yes'),
                Radio(
                  value: 'Female',
                  groupValue: empolye,
                  onChanged: (value) {
                    setState(() {
                      empolye = value.toString();
                    });
                  },
                ),
                Text('No'),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: designationNameController,
              // onChanged: contactController.name,
              decoration: InputDecoration(
                // focusedBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.purple, width: 2.0),
                // ),
                // enabledBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.grey, width: 1.0),
                // ),
                labelText: 'Your Designation',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text('Occupation:'),
                Radio(
                  value: 'Business',
                  groupValue: occupation,
                  onChanged: (value) {
                    setState(() {
                      occupation = value.toString();
                    });
                  },
                ),
                Text('Business'),
                Radio(
                  value: 'Job Holder',
                  groupValue: occupation,
                  onChanged: (value) {
                    setState(() {
                      occupation = value.toString();
                    });
                  },
                ),
                Text('Job Holder'),
              ],
            ),
            Divider(
              thickness: 1,
              color: Colors.black54,
            ),
            SizedBox(
              height: 6,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  _selectedDate != null
                      ? _selectedDate.toString()
                      : 'No date selected!',
                  style: const TextStyle(fontSize: 15),
                ),
                GestureDetector(
                  onTap: ()=>_presentDatePicker(context),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFF90A4AE),
                    ),
                    height: 50,
                    width: 135,
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: () {},
                          icon: Icon(Icons.date_range_rounded),
                          color: Colors.white,
                        ),
                        Text(
                          'Barth Date',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
